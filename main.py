#!/usr/bin/python3
#                     ___           __  ___
#  _______ ________  / (_)__  ___ _(())/ (_)__  ___ _
# / __/ _ `/ __/ _ \/ / / _ \/ -_) _ `/ / / _ \/ _ `/
# \__/\_,_/_/  \___/_/_/_//_/\__/\_,_/_/_/_//_/\_, /
#         https://gitlab.com/carolinealing    /___/
#
# main.py
#

from diet import Diet

def category(value: float) -> str:
	"""
	Returns a category string based which range the specified value falls
	"""
	return \
		'Underweight (Severe thinness)' if value < 16.0 else \
		'Underweight (Moderate thinness)' if value < 16.9 else \
		'Underweight (Mild thinness)' if value < 18.4 else \
		'Normal range' if value < 24.9 else \
		'Overweight (Pre-obese)' if value < 29.9 else \
		'Obese (Class I)' if value < 34.9 else \
		'Obese (Class II)' if value < 39.9 else \
		'Obese (Class III)'

diet = Diet()

value: float = diet.get_BMI()

print(f'Age: {diet.get_age()}')
print(f'BMR: {diet.get_BMR():.2f}')
print(f'BMI: {value:.1f} = {category(value)}')

diet.put_weight(13)
