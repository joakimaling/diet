#                     ___           __  ___
#  _______ ________  / (_)__  ___ _(())/ (_)__  ___ _
# / __/ _ `/ __/ _ \/ / / _ \/ -_) _ `/ / / _ \/ _ `/
# \__/\_,_/_/  \___/_/_/_//_/\__/\_,_/_/_/_//_/\_, /
#         https://gitlab.com/carolinealing    /___/
#
# diet/__init__.py
#

from datetime import date
from typing import Any, TypeAlias
import csv, yaml

# Data type for variable holding data
Type: TypeAlias = 'dict[str, Any]'

# Factor of activity level used in calculations
_ACTIVITY: tuple = (1.2, 1.375, 1.55, 1.725, 1.9)

# File where data for modelling/tracking is stored
_DATA_FILE: str = './data.yaml'

# File where basic, constant user data is stored
_USER_FILE: str = './user.yaml'

class Diet:
	"""
	"""

	def __init__(self) -> None:
		"""
		"""
		self.data = self._file_read()

		# If file couldn't be read - query the user
		if self.data == {}:
			self.data = self._data_read()

	def get_age(self) -> int:
		"""
		Calculates the age based on the specified birthdate.
		"""
		now: date = date.today()

		day: int = self.data['birth'].day
		month: int = self.data['birth'].month
		year: int = self.data['birth'].year

		return now.year - year - (now.month < month or now.day < day)

	def get_BMR(self) -> float:
		"""
		Calculates the Basal Metabolic Rate (BMR).

		Uses the Mifflin-St. Jeor equation and multiplies it by an activity
		factor. The unit is kcal/day.
		
		Source: https://en.wikipedia.org/wiki/Basal_metabolic_rate
		"""
		age: int = self.get_age()
		factor: float = self.data['activity']
		gender: int = -161 if self.data['gender'] == 'f' else 5
		height: float = self.data['height']
		weight: float = self.data['weight']

		return factor * (10.0 * weight + 106.25 * height - 5.0 * age + gender)

	def get_BMI(self) -> float:
		"""
		Calculates the Body Mass Index (BMI).

		Source: https://en.wikipedia.org/wiki/Body_mass_index
		"""
		height: float = self.data['height']
		weight: float = self.data['weight']

		return weight / (height * height)

	def put_weight(self, weight: float) -> None:
		"""
		Stores specified weight along with today's date for modelling
		"""
		with open(_DATA_FILE, 'a') as file:
			writer = csv.writer(file)
			
			writer.writerow([date.today(), weight])

	def _data_read(self) -> Type:
		"""
		Reads data from user, stores it in a file and returns it.
		"""
		data: Type = {}
		
		# Query basic information 
		data['birth'] = date.fromisoformat(input('Birthdate (yyyy-mm-dd): '))
		data['height'] = float(input('Height (cm): ')) / 100.0
		data['weight'] = float(input('Weight (kg): '))
		data['gender'] = input('Gender (f/m): ')

		# Let user pick a level of activity
		pick: str = input('How active are you?\n\n1. Sedentary \n2. Lightly active \n3. Moderately active\n4. Active\n5. Very active\n\n> ')
		data['activity'] = _ACTIVITY[int(pick) - 1]

		# Store the collected data
		self._file_write(data)

		# Then, return it
		return data

	def _file_read(self) -> Type:
		"""
		Reads a file containing data stored in the YAML format.
		"""
		try:
			with open(_USER_FILE, 'r') as file:
				try:
					return yaml.safe_load(file)
				except yaml.YAMLError as error:
					print(f'YAML error: {error}')
					return {}
		except FileNotFoundError:
			return {}

	def _file_write(self, data: Type) -> None:
		"""
		Writes specified data to file in the YAML format.
		"""
		with open(_USER_FILE, 'w') as file:
			try:
				yaml.dump(data, file)
			except yaml.YAMLError as error:
				print(f'YAML error: {error}')
